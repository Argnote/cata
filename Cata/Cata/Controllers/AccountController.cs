﻿using Cata.Models;
using Cata.Services;
using Microsoft.AspNetCore.Mvc;

namespace Cata.Controllers
{
    [ApiController]
    [Route("Account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet("{id}",Name = "GetTransactions")]
        [ProducesResponseType(typeof(IEnumerable<Transaction>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetTransactions(int id)
        {
            (int code, IEnumerable<Transaction> response) = await _accountService.GetTransactions(id);
            if (code != StatusCodes.Status200OK) return StatusCode(code);
            return Ok(response);
        }

        [HttpPost(Name = "AddTransaction")]
        [ProducesResponseType(typeof(Transaction), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddTransactions(Transaction transaction)
        {
            (int code, Transaction response) = await _accountService.AddTransaction(transaction);
            if (code != StatusCodes.Status201Created) return StatusCode(code);
            return StatusCode(StatusCodes.Status201Created, response);
        }
    }
}
