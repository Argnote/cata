﻿using Cata.Data;
using Cata.Models;

namespace Cata.Services
{
    public class AccountService : IAccountService
    {
        private readonly CataContext _ctx;
        public AccountService(CataContext context)
        {
            _ctx = context;
        }
        public async Task<(int,IEnumerable<Transaction>)> GetTransactions(int id)
        {
            
            try
            {
                // Retrieve transaction for user id
                IEnumerable<Transaction> transaction = _ctx.Transactions.Where(u => u.Recipient == id || u.Sender == id);

                //reverses the amount of transactions for sender
                transaction.Where(x => x.Sender == id).Select(t => { t.Amount = -t.Amount; return t; }).ToList();

                return (StatusCodes.Status200OK, transaction);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return (StatusCodes.Status500InternalServerError, new List<Transaction>());
            }
        }

        public async Task<(int, Transaction)> AddTransaction(Transaction transaction)
        {
            try
            {
                //Add new tansaction in db
                _ctx.Transactions.Add(transaction);
                _ctx.SaveChanges();
                return (StatusCodes.Status201Created, transaction);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return (StatusCodes.Status500InternalServerError, new Transaction());
            }
        }
    }
}
