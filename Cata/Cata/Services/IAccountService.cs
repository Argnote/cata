﻿using Cata.Models;

namespace Cata.Services
{
    public interface IAccountService
    {
        public Task<(int, IEnumerable<Transaction>)> GetTransactions(int id);
        public Task<(int, Transaction)> AddTransaction(Transaction transaction);
    }
}
