﻿using Cata.Models;
using Microsoft.EntityFrameworkCore;

namespace Cata.Data
{
    public class CataContext : DbContext
    {

        public CataContext(DbContextOptions<CataContext> options)
            : base(options)
        { }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.Property(e => e.Id).IsUnicode(false);

                entity.Property(e => e.DateTime).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Amount).IsUnicode(false);

                entity.Property(e => e.Sender).IsUnicode(false);

                entity.Property(e => e.Recipient).IsUnicode(false);

            });

        }
    }
}
