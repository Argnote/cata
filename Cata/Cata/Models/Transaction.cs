﻿namespace Cata.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public DateTime DateTime { get; set; }

        public string? Description { get; set; }

        public int Amount { get; set; }

        public int? Sender { get; set; }

        public int? Recipient { get; set; }
        

    }
}
