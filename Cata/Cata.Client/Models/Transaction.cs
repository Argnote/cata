﻿using System.ComponentModel.DataAnnotations;

namespace Cata.Client.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public DateTime DateTime { get; set; }

        public string? Description { get; set; }

        [Required]
        [Range(1, Double.PositiveInfinity, ErrorMessage="la valeur minimum est 1")]
        public int Amount { get; set; }

        public int? Sender { get; set; }

        public int? Recipient { get; set; }
        

    }
}
