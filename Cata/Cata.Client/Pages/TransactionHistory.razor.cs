using Cata.Client.Models;
using Microsoft.AspNetCore.Components;
using System.Net.Http.Json;

namespace Cata.Client.Pages
{
    public partial class TransactionHistory : ComponentBase
    {
        [Inject] private HttpClient Http { get; set; }

        private IEnumerable<Transaction> transactions;
        private int amount;
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            //On init call API for retrieve transactions
            transactions = await Http.GetFromJsonAsync<IEnumerable<Transaction>>("https://localhost:5000/account/1");
            amount = transactions.Sum(t => t.Amount);
        }
    }
}
