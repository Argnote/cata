﻿using Cata.Client.Models;
using Microsoft.AspNetCore.Components;
using System.Net.Http.Json;

namespace Cata.Client.Components
{
    public partial class AddTransaction : ComponentBase
    {
        [Inject] NavigationManager manager {get; set;}
        [Inject] private HttpClient Http { get; set; }
        [Parameter] public int? Sender { get; set; }
        [Parameter] public int? Recipient { get; set; }
        private Transaction transaction = new();

        private async void HandleValidSubmit()
        {
            transaction.Sender = Sender;
            transaction.Recipient = Recipient;
            transaction.DateTime = DateTime.Now;

            //send transaction to API
            var result = await Http.PostAsJsonAsync<Transaction>("https://localhost:5000/account", transaction);
            manager.NavigateTo("/");
        }
    }
}
